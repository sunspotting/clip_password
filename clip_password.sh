#! /bin/bash
typo="off"
echo "Enter your handle"
read -s handle
echo "Enter your pwd"
read -s pass
while true; do
  clear
  echo "Password for: ${handle} (${typo})"
  echo "Hit enter to copy pwd into clipboard, 'n' to clear clipboard"
  read gort
  if [ "$gort" == "n" ]
  then
    echo "doh" |  xclip -selection c
    typo="off"
  else
    echo ${pass} |  xclip -selection c
    typo="on"
  fi
done
